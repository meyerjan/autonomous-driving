import tensorflow as tf
from enum import Enum
class ColorSpace(Enum):
	""" Defines the colorspaces used for training.
	"""

	YUV = (0, 3)
	HSV = (1, 3)
	RGB = (2, 3)
	GRAYSCALE = (3, 1)

	def __init__(self, index, num_channels):
		self.index = index
		# number of channels in the color-space
		self.num_channels = num_channels

	def convert(self, image):
		""" Function for converting an rgb/grayscale image to the given colorspace.
			Arguments:
				image: image either in rgb or grayscale.
			Returns:
				image converted to this colorspace.
		"""
		if self is ColorSpace.RGB:
			return image
		elif self is ColorSpace.YUV:
			return tf.image.rgb_to_yuv(image)
		elif self is ColorSpace.HSV:
			return tf.image.rgb_to_hsv(image)
		elif self is ColorSpace.GRAYSCALE:
			return image
			#return tf.image.rgb_to_grayscale(image)
