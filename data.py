import os
import random
import sys

import tensorflow as tf
import numpy as np

from color_space import ColorSpace

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

class Packer():
	""" Packer for loading the image - steering wheel angle pairs, splitting them into training and validation sets,
		pruning and augmenting the training set, calculating and storing the mean and std of the training set, 
		cropping and downsampling the images, and storing them into .tfrecord-files ready for training.
	"""

	def __init__(self, color_space=ColorSpace.YUV, transform=None, augmentations=None):
		"""	Initialize the packer.
			Arguments:
				color_space: the color space the images should be stored in/the mean and std should be calculated in
				transform: preprocessing step for the images before storing them, usually cropping
				augmentations: augmentations of the training dataset
		"""
		self.color_space = color_space
		self.transform = transform
		self.augmentations = augmentations

	def pack(self, root="data", output="data", training=[], validation=[], index_file="index.txt"):
		""" pack the image-steering wheel angle pairs found in all trips in the root-folder.
			Arguments:
				root: path to the folder containing all the trips to be packed.
				output: path to output where the tfrecord-files and mean+std should be stored.
				training: optionally predefined training set.
				validation: optionally predefined validation set.
				index_files: defines the dataset to be used. name of the file that matches the steering wheel angles to images.
		"""
		tf.enable_eager_execution()

		# if no training set is pre-defined use every trip in the root folder, except ones already in the validation set.
		if not training:
			training = [folder for folder in os.listdir(root) if os.path.isdir(os.path.join(root, folder)) and not folder in validation]

		# read the index files. records is a dictionary of { folder: records } with an entry for every trip in the training and validation lists
		records, num_records = read_index_files(root, training + validation, index_file)
		# determine the size of the validation set with a split of 80/20
		len_val = 0.2 * num_records
		for folder in validation:
			len_val = len_val - records[folder][1]
		# randomly add trips from training to validation until validation size has been reached
		while len_val > 0:
			folder = random.choice(training)
			training.remove(folder)
			validation.append(folder)
			len_val = len_val - records[folder][1]

		records = prune(records, training)

		mean = np.zeros((66, 200, self.color_space.num_channels))

		print("Writing Training images...")
		path = os.path.join(output, "training.tfrecord")
		num_records = self.write_records(path, records, training, mean=mean, augment=True)

		# calculate and store mean of training set
		mean = mean / (num_records * (len(self.augmentations) + 1))
		path = os.path.join(output, "mean.npy")
		write_array(path, mean)

		# calculate and store std of training set
		path = os.path.join(output, "std.npy")
		self.write_std(path, records, training, mean)

		print("Writing Validation images...")
		path = os.path.join(output, "validation.tfrecord")
		self.write_records(path, records, validation)

	def write_records(self, path, records, folders, mean=None, augment=False):
		""" Store all records in the given records dictionary under the key words listed in folders.
			Arguments:
				path: path to .tfrecord output.
				records: all records in a dictionary.
				folders: trips to be stored in this dataset.
				mean: if not none: aggregate for mean calculation.
				augment: if not false: augment the stored images using self.augmentation.
			Returns:
				number of records stored in the tfrecord
		"""
		writer = tf.python_io.TFRecordWriter(path)
		num_records = 0
		for folder in folders:
			for record in records[folder][0]:
				image = self.load_image(record[0])
				if mean is not None:
					self.calculate_mean(mean, image) # aggregate in mean var
				write_record(writer, image, record[1])

				if augment: # augment the set using every function in self.augmentation
					for augmentation in self.augmentations:
						_image, _label = augmentation(image, record[1])
						if mean is not None:
							self.calculate_mean(mean, _image)
						write_record(writer, _image, _label)
			num_records = num_records + records[folder][1]
			print("Loaded {0} records from {1}.".format(records[folder][1], folder))
		writer.close()
		print("Written {0} records to {1}.".format(num_records, path))
		if augment and self.augmentations:
			print("Written {0} augmented records to {1}.".format(num_records * len(self.augmentations), path))
		return num_records

	def load_image(self, file):
		""" load the jpeg stored in the given file.
			Arguments:
				file: path to the image.jpg
			Returns:
				loaded image.
		"""
		image_raw = tf.read_file(file)
		# Decodes the image either as rgb or grayscale, depending on num_channels.
		image = tf.image.decode_jpeg(image_raw, channels=self.color_space.num_channels)
		image = tf.image.resize_images(image, [800, 1280]) # original size

		# transformations: usually cropping
		if self.transform is not None:
			image = self.transform(image)

		image = tf.image.convert_image_dtype(image, tf.float32)
		image = tf.image.resize_images(image, [66, 200]) # input size of neural network
		image = tf.image.convert_image_dtype(image, tf.uint8)
		return image

	def calculate_mean(self, mean, image):
		""" Add image to the mean aggregate.
			Arguments:
				mean: aggregate.
				image: image.
			Returns:
				mean aggregate.
		"""
		image = tf.image.convert_image_dtype(image, tf.float32)
		image = self.color_space.convert(image)
		mean += image.numpy()
		return mean

	def calculate_std(self, std, image, mean):
		""" Add image to the std aggregate-
			Arguments:
				std: std aggregate.
				image: image.
				mean: calculated mean of the dataset.
			Returns:
				std aggregate.
		"""
		image = tf.image.convert_image_dtype(image, tf.float32)
		image = self.color_space.convert(image)
		image = (image - mean)**2
		return std + image

	def write_std(self, path, records, folders, mean):
		""" Write and calculate the std of the records with keys in folders. 
			Arguments:
				path: path to the output file for the std.
				records: all records.
				folders: trips used in the std calculation (usually training dataset).
				mean: calculated mean of the dataset.
		"""
		num_records = 0
		std = np.zeros((66, 200, self.color_space.num_channels))
		for folder in folders:
			for record in records[folder][0]:
				image = self.load_image(record[0])
				std = self.calculate_std(std, image, mean)

				for augmentation in self.augmentations:
					_image, _ = augmentation(image, 0)
					std = self.calculate_std(std, _image, mean)
			num_records = num_records + records[folder][1]
		std = std / (num_records * (len(self.augmentations) + 1))
		std = np.sqrt(std)
		write_array(path, std)

def prune(records, folders, cutoff=10000):
	""" Prune the dataset defined by the trips in folders.
		Arguments:
			records: all records.
			folders: trips used in pruning (usually training dataset).
			cutoff: cutoff for each bag.
		Returns:
			pruned dataset.
	"""

	seperators = np.arange(-1.95, 2.0, 0.1)
	distribution = [[] for i in range(len(seperators) + 1)]
	# sort the steering wheel angles into bags.
	for folder in folders: 
		for record in records[folder][0]:
			hit = False
			for i, s in enumerate(seperators):
				if record[1] < s:
					distribution[i].append((folder, record))
					hit = True
					break
			if not hit:
				distribution[-1].append((folder, record))
		records[folder] = []
	# sample from the bags with the given cutoff
	for bag in distribution:
		count = 0
		while len(bag) > 0 and count < cutoff:
			(folder, record) = random.choice(bag)
			bag.remove((folder, record))
			records[folder].append(record)
			count += 1
	for folder in folders:
		records[folder] = (records[folder], len(records[folder]))
	return records

def crop_image(image):
	""" Crop the given image.
		Arguments:
			image: image to be cropped.
		Returns:
			cropped image.
	"""

	# image = tf.image.crop_to_bounding_box(image, 200, 0, 400, 1280) # cropping (1) as per the thesis
	image = tf.image.crop_to_bounding_box(image, 300, 50, 270, 1180) # cropping (2) as per the thesis
	# image = tf.image.crop_to_bounding_box(image, 400, 200, 150, 880) # cropping (3) as per the thesis
	return image

def read_index_files(root, folders, index_file):
	""" Load alle records in the given trips per the dataset defined in index_file.
		Arguments:
			root: root folder containing the trips to be read.
			folders: all the trips to be read.
			index_file: index_file containing all steering wheel angle - image pairs for a trip in the desired dataset.
		Returns:
			Dictionary containing all loaded records sorted by their trips.
			Number of records loaded.
	"""
	records = {}
	num_records = 0
	for folder in folders:
		path = os.path.join(root, folder)
		if not os.path.isdir(path):
			print("Folder {0} not found.".format(path))
			continue

		X = read_index_file(path, index_file)
		records[folder] = (X, len(X))
		num_records += records[folder][1]
	return records, num_records

def read_index_file(path, index_file):
	""" Load all records in the given trip.
		Arguments
			path: complete path to the trip
			index_file: index_file defining the dataset to be used. matching steering wheel angles with images.
		Returns:
			List of all records loaded.
	"""
	index = os.path.join(path, index_file)
	if not os.path.isfile(index):
		print("Index file {0} not found".format(index))
		return []

	images = os.path.join(path, "images")
	X = []
	with open(index, 'r') as f:
		for line in f:
			xs = line.strip().split(' ')
			X.append((os.path.join(images, "{0}.jpg".format(xs[0])), float(xs[1])))
	print("Read index file {0}.".format(index))
	return X

def flip_record(image, label):
	""" Create mirror version of the record. Agumentation function.
		Arguments:
			image: image to be mirrored.
			label: steering wheel angle to be inverted.
		Returns:
			flipped image, flipped label
	"""
	return tf.image.flip_left_right(image), -label

def write_array(path, array):
	""" Write the numpy array to the given path (used for mean and std.)
		Arguments:
			path: complete path to the output file.
			array: array to be stored.
	"""
	np.save(path, array)
	print("Written array to {0}.".format(path))

def write_record(writer, image, label):
	""" Write a single record to a rfrecord using the writer.
		Arguments:
			writer: TFRecordWriter used to write the record.
			image: image in the record.
			label: steering wheel angle in the record.
	"""
	img = tf.image.encode_jpeg(image)
	feature = {
		"image": tf.train.Feature(bytes_list=tf.train.BytesList(value=[img.numpy()])), 
		"label": tf.train.Feature(float_list=tf.train.FloatList(value=[label]))
		}
	example = tf.train.Example(features=tf.train.Features(feature=feature))
	writer.write(example.SerializeToString())

if __name__ == "__main__":
	if len(sys.argv) > 1:
		root = sys.argv[1]
	else:
		print("Using default root in /data")
		root="data"

	packer = Packer(color_space=ColorSpace.GRAYSCALE, transform=crop_image, augmentations=[flip_record])
	packer.pack(root=root, output=root, validation=[], index_file="index.txt")