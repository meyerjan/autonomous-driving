import os
import sys
import cv2

from data import read_index_file

def view_trip(path, index_file="index.txt"):
	"""	View the trip stored in the given path.
		path: path to the folder in which the trip is stored
		index_file: name of the index file holding the timestamps and steering wheel angles of each image
	"""
	records = read_index_file(path, index_file)

	for image, _ in records:
		img = cv2.imread(image)
		cv2.imshow("img", img)
		cv2.waitKey(5)


if __name__ == "__main__":
	if len(sys.argv) < 2:
		print("Missing argument: path to trip")
		exit()
	path = sys.argv[1]
	if len(sys.argv) > 2:
		index_file=sys.argv[2]
	else:
		index_file="index.txt"
	view_trip(path=path, index_file=index_file)