import sys
import os

import numpy as np
import tensorflow as tf
import cv2

from model import Model
from color_space import ColorSpace
from data import read_index_file

class Visualizer():
	""" Visualize the model predictions and VisualBackProp visualization masks.
	"""

	def __init__(self, model_path="model", model_name="model", batch_size=128, color_space=ColorSpace.YUV):
		""" Init the visualization.
			Arguments:
				model_path: path to the model to be visualized.
				model_name: name of the model.
				batch_size: batch size to be used.
				color_space: colorspace of the model.
		"""
		self.model_path = model_path
		self.model_name = model_name
		self.batch_size = batch_size
		self.color_space = color_space

	def predict(self, folder, index_file="index.txt"):
		""" Create predictions and visualization masks.
			Arguments:
				folder: path to the folder of the trip to be visualized.
				index_file: index-file/dataset to be used for visualization.
			Returns:
				images used for visualization
				corresponding steering wheel angles to the images
				visualization masks
				network predictions
		"""
		records = read_index_file(folder, index_file)
		files, labels = zip(*records)
		# keine tfrecords sondern direkt aus den files.
		dataset = tf.data.Dataset.from_tensor_slices((list(files), list(labels)))
		dataset = dataset.apply(tf.contrib.data.map_and_batch(map_func=self.parse_image, batch_size=self.batch_size, drop_remainder=True))
		dataset = dataset.prefetch(buffer_size=100)
		iterator = dataset.make_one_shot_iterator()

		x, y = iterator.get_next()
		model = Model(batch_size=self.batch_size, color_space=self.color_space)
		model.build(x=x, y=y, training=tf.constant(False), drop_rate=tf.constant(0.0))

		predictions = []
		masks = []
		with tf.Session() as session:
			model.restore(session, os.path.join(self.model_path, "{0}.ckpt".format(self.model_name)))
			session.run(tf.local_variables_initializer())
			while True:
				try:
					y_pred, mask, _ = session.run((model.y_pred, model.visualization_mask, model.metrics))
					for m, pred in zip(mask, y_pred):
						masks.append(np.reshape(m.astype(np.uint8), (66, 200)))
						predictions.append(pred[0])
				except tf.errors.OutOfRangeError:
					break
			loss = model.mean_loss.eval()
			error = model.mean_error.eval()
			acc = model.acc.eval()
			print("loss: {0} - error: {1} - acc: {2}".format(loss, error, acc))
		return files, labels, masks, predictions

	def visualize(self, image_files, labels, masks, predictions):
		""" Visualize the results as an image stream of the predictions/masks.
			Arguments:
				image_files, labels, masks, predicions: returns of the predict function.
		"""
		self.angles = [0.0] * 100
		self.angles_pred = [0.0] * 100
		self.angles_idx = 0

		for image_file, label, mask, prediction in zip(image_files, labels, masks, predictions):
			cv2.imshow("mask", mask)

			image = cv2.imread(image_file)
			# These depend on the cropping to be used. Adjust as neccessary. Here: Cropping (2)
			mask = cv2.resize(mask, (1180, 270))
			image[300:570,50:-50, 1] = cv2.add(image[300:570,50:-50, 1], mask)
			image = self.draw_crop_box(image, [300, 50, 270, 1180)

			image = self.draw_steering(image, label, prediction)
			cv2.imshow("image", image)
			cv2.waitKey(15)

	def draw_steering(self, image, angle, angle_pred):
		""" Visualize the steering prediction aganst the ground truth.
		"""

		y = 800
		self.angles[self.angles_idx] = angle
		self.angles_pred[self.angles_idx] = angle_pred
		self.angles_idx = (self.angles_idx + 1) % len(self.angles)

		cv2.line(image, (1100, y), (1100, y - 200), (0, 0, 255), 1)
		for i in range(len(self.angles) - 1):
			cv2.line(image, (int(1100 - self.angles[(self.angles_idx + i) % len(self.angles)] * 100.0), y),
				(int(1100 - self.angles[(self.angles_idx + i + 1) % len(self.angles)] * 100.0), y - 2), (0, 0, 255), 1)
			cv2.line(image, (int(1100 - self.angles_pred[(self.angles_idx + i) % len(self.angles_pred)] * 100.0), y),
				(int(1100 - self.angles_pred[(self.angles_idx + i + 1) % len(self.angles_pred)] * 100.0), y - 2), (255, 0, 0), 1)
			y = y - 2
		return image

	def draw_crop_box(self, image, bbox):
		""" Draw the cropping bounding box.
		"""
		cv2.line(image, (bbox[1], bbox[0]), (bbox[1] + bbox[3], bbox[0]), (0, 0, 255), 1)
		cv2.line(image, (bbox[1], bbox[0]), (bbox[1], bbox[0] + bbox[2]), (0, 0, 255), 1)
		cv2.line(image, (bbox[1] + bbox[3], bbox[0]), (bbox[1] + bbox[3], bbox[0] + bbox[2]), (0, 0, 255), 1)
		cv2.line(image, (bbox[1], bbox[0] + bbox[2]), (bbox[1] + bbox[3], bbox[0] + bbox[2]), (0, 0, 255), 1)
		return image

	def parse_image(self, file, label):
		""" Parse a record for network consumption.
		"""

		f = tf.read_file(file)
		image = tf.image.decode_jpeg(f, channels=self.color_space.num_channels)
		image = tf.image.crop_to_bounding_box(image, 300, 50, 270, 1180) # Cropping used during training, here: cropping(2)
		image = tf.image.resize_images(image, [66, 200])
		image = image / tf.constant(255.0)
		image = self.color_space.convert(image)
		return image, [label]

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print("Missing argument: path/to/trip.")
		exit()
	
	folder = sys.argv[1]

	visualizer = Visualizer(model_path="model", model_name="model", color_space=ColorSpace.GRAYSCALE)
	images, labels, masks, predictions = visualizer.predict(folder=folder, index_file="index.txt")
	visualizer.visualize(images, labels, masks, predictions)