import os
import time

import matplotlib.pyplot as plt

class Logger():
	""" Logger for training.
	"""
	def __init__(self, file_name, path="logs"):
		""" Init logger.
			Arguments:
				file_name: name of the log file.
				path: path to the log file.
		"""
		if not os.path.exists(path):
			os.makedirs(path)

		self.path = path
		self.file_name = file_name
		self.entries = []
		self.history = {'epoch': [], 'training_loss': [], 'training_error': [], 'training_acc': [], 'validation_loss': [], 'validation_error': [], 'validation_acc': [], 'learning_rate': []}
		self.log_format = "Epoch {0:>2}/{1} - {2:>2}s - train loss: {3:<6.4} - train mae: {4:<6.4} - train acc: {5:<6.4} - val loss: {6:<6.4} - val mae: {7:<6.4} - val acc: {8:<6.4}"

	def log(self, entry):
		""" Create entry in log file and console output.
			Arguments:
				entry: log entry.
		"""
		self.entries.append(entry + "\n")
		print(entry)

	def set_comment(self, comment):
		""" Set a comment to be output before training.
			Arguments:
				comment: comment to be printed before training, for example specifying the configuration further.
		"""
		self.comment = comment

	def start_training(self, num_epochs, num_training, num_validation, lr, reg, color_space):
		""" Log the start of the training.
		 	Arguments:
			 	num_epochs: number of epochs the training is going to last.
				num_training: number of samples for training.
				num_validation: number of samples for validation.
				lr: learning rate used in training.
				reg: weight of weight decay used in training.
				color_space: color format used in training.
		"""
		self.num_epochs = num_epochs
		self.log("Starting Training on {0} training images and {1} validation images for {2} epochs.".format(num_training, num_validation, self.num_epochs))
		self.log("Learning rate: {0}; regularization: {1}".format(lr, reg))
		self.log("Color Space: {0}".format(color_space))
		self.log(self.comment)
		self.time_training = time.time()

	def end_training(self, model_path, validation_loss, validation_error, validation_acc):
		""" Log the end of training.
			Arguments:	
				model_path: path to the trained model.
				validation_loss: validation loss on the best performing model.
				validation_error: validation mae on the best performing model.
				validation_acc: validation accuracy on the best performing model.
		"""
		self.log("Model saved to {0} with validation loss: {1}; error: {2}; acc: {3}".format(model_path, validation_loss, validation_error, validation_acc))
		self.log("Total Elapsed Time: {0}s".format(int(time.time() - self.time_training)))

	def start_epoch(self, epoch):
		""" Log the start of an epoch.
			Arguments:
				epoch: Number of the epoch.
		"""
		self.current_epoch = epoch
		self.time_epoch = time.time()

	def end_epoch(self, epoch, train_loss, train_error, train_acc, validation_loss, validation_error, validation_acc):
		""" Log the end of an epoch.
			Arguments:
				epoch: number of the epoch.
				train_loss: loss on training set for epoch.
				train_error: mae on training set for epoch.
				train_acc: accuracy on training set for epoch.
				validation_loss: loss on validation set for epoch.
				validation_error: mae on validation set for epoch.
				validation_acc: accuracy on validation set for epoch.
		"""
		if self.current_epoch != epoch:
			self.log("Error logging epoch: start {0} and end {1} dont match.".format(self.current_epoch, epoch))
		duration = int(time.time() - self.time_epoch)

		self.history['epoch'].append(epoch)
		self.history['training_loss'].append(train_loss)
		self.history['training_error'].append(train_error)
		self.history['training_acc'].append(train_acc)
		self.history['validation_loss'].append(validation_loss)
		self.history['validation_error'].append(validation_error)
		self.history['validation_acc'].append(validation_acc)

		entry = self.log_format.format(epoch, self.num_epochs, duration, train_loss, train_error, train_acc, validation_loss, validation_error, validation_acc)
		self.log(entry)

	def write_log(self):
		""" Write the log to file.
		"""
		txt_file = os.path.join(self.path, "{0}.txt".format(self.file_name))
		with open(txt_file, 'w') as f:
			f.writelines(self.entries)

		plot_file = os.path.join(self.path, "{0}.png".format(self.file_name))
		self.plot_history()
		plt.tight_layout()
		plt.savefig(plot_file)
		plt.clf()

	def plot_history(self):
		""" Plot and save the training history to file.
		"""
		plt.figure(1)

		plt1 = plt.subplot(3, 1, 1)
		plt.title('Loss over Epoch')
		plt.xlabel('Epoch')
		plt.ylabel('Loss')
		plt.plot(self.history['epoch'], self.history['training_loss'], label='Train loss')
		plt.plot(self.history['epoch'], self.history['validation_loss'], label='Val loss')
		plt.legend()

		plt.subplot(3, 1, 2, sharex=plt1)
		plt.title('Mean Absolute Error over Epoch')
		plt.xlabel('Epoch')
		plt.ylabel('Mean Abs Error')
		plt.plot(self.history['epoch'], self.history['training_error'], label='Train error')
		plt.plot(self.history['epoch'], self.history['validation_error'], label='Val error')
		plt.legend()

		plt.subplot(3, 1, 3, sharex=plt1)
		plt.title('Accuracy over Epoch')
		plt.xlabel('Epoch')
		plt.ylabel('Accuracy')
		plt.plot(self.history['epoch'], self.history['training_acc'], label='Train acc')
		plt.plot(self.history['epoch'], self.history['validation_acc'], label='Val acc')
		plt.legend()

