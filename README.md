# End-To-End Learning of Steering Wheel Angles for Autonomous Driving

Project written and developed in conjunction with my bachelor thesis "End-to-End
Learning of Steering Wheel Angles for Autonomous Driving".

## Extraction from bag-files
The extraction of front camera images and steering wheel angles is performed 
with ROS.  
Starting Extract.launch with arguments 
> bag:=/path/to/bag-file folder:=/output/path 

will start a launch-file that plays the specified bag file, extracts the images 
recorded by the front view camera and the steering wheel angles, synchronizes 
them and stores them in the specified folder:  
* images are stored in /folder/images/ as .jpgs with their timestamp as name  
* steering wheel angles are matched with images in /folder/index.txt  

To extract the rectified images instead, change `image_raw` in line 43 of 
/scripts/listener.py to `image_rect`.

## FilterTool
The filter tool can be used to create alternative datasets instead of simply 
using all records.  
Alternative datasets are defined in alternative text-files to the default 
index.txt, containing only those image-steering wheel angle pairs wanted in the 
dataset. Usage:
> .\FilterTool.exe path/to/trips

![FilterTool](FilterTool/screen.png)

This will allow the viewing of all trips contained in path/to/trips. The trips 
are shown in succession, images that are to be included in the new dataset will 
be written to filtered.txt in the current trips folder. Whether the current file 
is written or not can be seen in the top left corner of the App, which can be 
toggled with the `A`-key. Pause/Resume the playback with the `W`-key. 
The next-frame and previous-frame buttons at the bottom will go forward/backward
for a single frame, the next and previous button will go to the next/previous 
trip in the path/to/trips folder. Going backward with the buttons or with the 
timeline in the middle will erase already written records. The bottom right 
shows the steering wheel angle.

## view.py
A simply script to view a single trip. Usage:
> py view.py /path/to/trip [index-file.txt]

## data.py
Creates the tfrecord files used in training. What dataset is to be used, is 
again defined by the index-file. Loads all records in the dataset, creates the 
training-validation split, optionally crops the images, downsamples them to 
200x66, calculates the mean and std of the training set and prunes the training 
set. Basically performs all steps necessary before training. Usage:
> py data.py [/path/to/trips]

Adjust the code for further changes to the configuration.

## train.py
Trains the model using the tfrecord files created with data.py. Usage:
> py data.py [/path/to/folder-of-tfrecords]

Adjust the code for further changes to the configuration.

## visualize.py
Visualize the Training results with VisualBackProp. Usage:
> py visualize.py [/path/to/trip]

Code needs to be adjusted depending on the configuration of the training that 
produced the model, i.e. for cropping.