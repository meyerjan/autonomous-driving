#!/usr/bin/env python
import rospy
import rospkg
from sensor_msgs.msg import Image
from fub_mig_can_msgs.msg import MIGSteerAssistStatus
import numpy as np
from cv_bridge import CvBridge
import cv2
import message_filters
import os
import sys

class ImageWheelListener:
	""" Listens on image_raw/image_rect and steer_assist_status for front view images and steering wheel angles,
		synchronizes them and saves them in the folder specified by the first command line argument.
		/folder 
			/images 
				front view images stored as jpgs with their timestamp as name
			index.txt - matches steering wheel angles with images per their timestamp
	""" 
    def callback(self, image, wheel):   
        time = image.header.stamp
		# Take about 5 samples per second
        if self.last is None or (time - self.last).to_sec() >= 0.18:
            img = self.bridge.imgmsg_to_cv2(image, image.encoding)
            cv2.imwrite("{0}/images/{1}.jpg".format(self.folder, str(time)), img)

            with open("{0}/index.txt".format(self.folder), 'a') as f:
                f.write("{0} {1} \n".format(str(time), str(wheel.steer_angle - 0.065)))
            self.last = time

    def __init__(self):
        self.bridge = CvBridge()
        rospack = rospkg.RosPack()
        self.folder = sys.argv[1]
        if not os.path.exists(self.folder + "/images"):
            os.makedirs(self.folder + "/images")

        self.last = None
        rospy.init_node("FrontCameraListener", anonymous=True)
        
		# alternatively change image_raw to image_rect for rectified images
        image_sub = message_filters.Subscriber("image_raw", Image)
        wheel_sub = message_filters.Subscriber("steer_assist_status", MIGSteerAssistStatus)

        ts = message_filters.ApproximateTimeSynchronizer([image_sub, wheel_sub], 1000, 0.1)
        ts.registerCallback(self.callback)
        rospy.spin()


if __name__ == '__main__':
    ImageWheelListener()
