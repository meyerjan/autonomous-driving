﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilterTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private string[] folders;

        private string srcIndex;
        private string dstIndex;
        private bool writing;
        private bool firstWrite;
        private bool paused;
        private bool sliderDrag;

        private bool sliderUpdated;
        private bool spaceDown;
        private int writingDots;

        private string[] lines;
        private int line;
        private int folder;
        private Stopwatch stopwatch;

        private double[] angles;
        private int anglesPos;

        public MainWindow(string root)
        {
            InitializeComponent();
            CompositionTarget.Rendering += RenderFrame;

            this.folders = Directory.GetDirectories(root);
            this.srcIndex = "index.txt";
            this.dstIndex = "filtered.txt";

            this.writing = true;

            this.stopwatch = new Stopwatch();
            this.stopwatch.Start();

            this.paused = true;
            this.sliderUpdated = false;
            this.sliderDrag = false;
            this.spaceDown = false;
            this.writingDots = 0;

            this.angles = new double[100];
            this.anglesPos = 0;

            this.folder = 0;

            this.loadFolder();
            this.loadImage();
        }

        protected void RenderFrame(object sender, EventArgs e)
        {
            this.Label.Visibility = this.writing ? Visibility.Visible : Visibility.Hidden;

            if (this.paused || this.sliderDrag)
                return;

            if (this.line >= this.lines.Length)
            {
                this.folder++;
                if (this.folder >= this.folders.Length)
                {
                    this.Close();
                    return;
                }

                this.loadFolder();

                if (this.lines.Length == 0)
                    return;
            }

            if (this.stopwatch.ElapsedMilliseconds < 25)
                return;
            this.stopwatch.Restart();

            this.loadImage();

            if (this.writing)
            {
                if (this.firstWrite)
                {
                    File.Delete($"{this.folders[this.folder]}/{this.dstIndex}");
                    this.firstWrite = false;
                }

                File.AppendAllText($"{this.folders[this.folder]}/{this.dstIndex}",
                    this.lines[this.line] + Environment.NewLine);

                this.writingDots = (this.writingDots + 1) % 40;
                this.Label.Content = "Writing" + string.Concat(Enumerable.Repeat(" .", this.writingDots / 10));
            }

            this.line++;
        }

        protected void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space && !this.spaceDown && !e.IsRepeat)
            {
                this.writing = !this.writing;
                this.spaceDown = true;
            }
            e.Handled = true;
        }

        protected void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.A)
            {
                this.writing = !this.writing;
            }
            else if (e.Key == Key.Right && !e.IsRepeat)
            {
                this.nextFolder();
            }
            else if (e.Key == Key.Left && !e.IsRepeat)
            {
                this.prevFolder();
            }
            else if (e.Key == Key.Escape && !e.IsRepeat)
            {
                this.Close();
            }
            else if (e.Key == Key.Space && this.spaceDown && !e.IsRepeat)
            {
                this.writing = !this.writing;
                this.spaceDown = false;
            }
            else if (e.Key == Key.W && !e.IsRepeat)
            {
                this.paused = !this.paused;
                this.PlayPauseImage.BeginInit();
                string image = this.paused ? "play" : "pause";
                this.PlayPauseImage.Source = new BitmapImage(new Uri($"textures/{image}.png", UriKind.Relative));
                this.PlayPauseImage.EndInit();
            }

            e.Handled = true;
        }

        private void loadFolder()
        {
            this.line = 0;
            try
            {
                this.lines = File.ReadAllLines($"{this.folders[this.folder]}/{this.srcIndex}");
            }
            catch (FileNotFoundException)
            {
                this.lines = new string[] { };
            }

            this.firstWrite = true;
            this.Title = $"{this.folders[this.folder]}";
        }

        private void loadImage()
        {
            if (this.lines.Length == 0)
                return;

            double progress = (this.line + 1.0) / this.lines.Length;
            this.Title = $"{this.folders[this.folder]} - {(int)(progress * 100.0f)}%";
            this.sliderUpdated = true;
            this.Slider.Value = progress;

            string[] split = this.lines[this.line].Split(' ');
            string imageName = split[0] + ".jpg";
            double angle = Double.Parse(split[1]);
            this.angles[this.anglesPos] = angle;
            this.anglesPos = (this.anglesPos + 1) % this.angles.Length;

            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.UriSource = new Uri($"{this.folders[this.folder]}/images/{imageName}");
            bmp.DecodePixelHeight = 800;
            bmp.EndInit();

            RenderTargetBitmap renderTarget = new RenderTargetBitmap(bmp.PixelWidth, bmp.PixelHeight, bmp.DpiX,
                bmp.DpiY, PixelFormats.Pbgra32);
            DrawingVisual visual = new DrawingVisual();
            using (DrawingContext context = visual.RenderOpen())
            {
                context.DrawImage(bmp, new Rect(0, 0, bmp.Width, bmp.Height));
                Pen pen = new Pen(Brushes.Red, 1.0);
                pen.DashStyle = new DashStyle(new Double[] { 2, 8 }, 0);
                double x = 1100.0;
                double y = 800.0;
                double yDelta = 200.0 / this.angles.Length;
                context.DrawLine(pen, new Point(x, y), new Point(x, y - yDelta * this.angles.Length));
                // pen = new Pen(Brushes.Blue, 1.0);
                // context.DrawLine(pen, new Point(x - 0.1 * 100, y), new Point(x - 0.1 * 100, y - yDelta * this.angles.Length));

                pen = new Pen(Brushes.Red, 1.0);
                for (int i = 0; i < this.angles.Length - 1; i++)
                {
                    context.DrawLine(pen,
                        new Point(x - this.angles[(this.anglesPos + i) % this.angles.Length] * 100, y),
                        new Point(x - this.angles[(this.anglesPos + i + 1) % this.angles.Length] * 100, y - yDelta));
                    y = y - yDelta;
                }
            }
            renderTarget.Render(visual);
            this.Image.Source = renderTarget;

            this.FilenameLabel.Content = imageName;
        }

        protected void OnSliderDragStarted(object sender, EventArgs e)
        {
            this.sliderDrag = true;
        }

        protected void OnSliderDragCompleted(object sender, EventArgs e)
        {
            this.sliderDrag = false;
            this.line = (int)((this.lines.Length - 1) * this.Slider.Value);
            this.loadImage();
            this.truncateDstIndex();
        }

        protected void OnSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.sliderUpdated)
            {
                this.sliderUpdated = false;
            }
            else
            {
                this.line = (int)((this.lines.Length - 1) * this.Slider.Value);
                this.loadImage();
                this.truncateDstIndex();
            }
        }

        protected void OnPlayPauseButtonClicked(object sender, RoutedEventArgs e)
        {
            this.paused = !this.paused;
            this.PlayPauseImage.BeginInit();
            string image = this.paused ? "play" : "pause";
            this.PlayPauseImage.Source = new BitmapImage(new Uri($"textures/{image}.png", UriKind.Relative));
            this.PlayPauseImage.EndInit();
        }

        protected void OnPrevFrameButtonClicked(object sender, RoutedEventArgs e)
        {
            if (this.line > 0)
            {
                this.line--;
                this.loadImage();
                this.truncateDstIndex();
            }
        }

        protected void OnPrevButtonClicked(object sender, RoutedEventArgs e)
        {
            this.prevFolder();
        }

        protected void OnNextFrameButtonClicked(object sender, RoutedEventArgs e)
        {
            if (this.line + 1 < this.lines.Length)
                this.line++;
            this.loadImage();
        }

        protected void OnNextButtonClicked(object sender, RoutedEventArgs e)
        {
            this.nextFolder();
        }

        private void prevFolder()
        {
            if (this.folder > 0)
            {
                this.folder--;
                this.loadFolder();
                while (this.lines.Length == 0 && this.folder > 0)
                {
                    this.folder--;
                    this.loadFolder();
                }

                this.loadImage();
            }

        }

        private void nextFolder()
        {
            this.folder++;
            if (this.folder >= this.folders.Length)
            {
                this.Close();
                return;
            }

            this.loadFolder();
            while (this.lines.Length == 0)
            {
                this.folder++;
                if (this.folder >= this.folders.Length)
                {
                    this.Close();
                    return;
                }

                this.loadFolder();
            }

            this.loadImage();
        }

        private void truncateDstIndex()
        {
            long current = Int64.Parse(this.lines[this.line].Split(' ')[0]);
            string fileName = $"{this.folders[this.folder]}/{this.dstIndex}";
            if (!File.Exists(fileName))
                return;
            File.WriteAllLines(fileName,
                File.ReadLines(fileName).Where(l => Int64.Parse(l.Split(' ')[0]) < current).ToList());
        }
    }
}
