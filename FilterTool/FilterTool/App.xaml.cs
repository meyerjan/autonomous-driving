﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace FilterTool
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            if(e.Args.Length < 1)
                Console.WriteLine("Missing argument: path/to/trips");
            else {
                MainWindow window = new MainWindow(e.Args[0]);
                window.Show();
            }
        }
    }
}
