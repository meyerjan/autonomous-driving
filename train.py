import sys
import os
import math

import tensorflow as tf
import numpy as np

from model import Model
from log import Logger
from color_space import ColorSpace

class Trainer():
	def __init__(self, model_path="model", model_name="model", color_space=ColorSpace.YUV, logger=None):
		""" Initialize the training.
			Arguments:
				model_path: path to the model, either for saving or storing.
				model_name: name of the model in the model_path for saving or storing.
				color_space: colorspace to be used in training.
				logger: Logger for logging to file and console, and creating plots of the training.
		"""
		self.model_path = model_path
		self.model_name = model_name
		self.color_space = color_space

		self.logger = logger
		if self.logger is None:
			if not os.path.exists("logs"):
				os.makedirs("logs")
			self.logger = Logger(file_name="log")
		# format of the records in the tfrecord files.
		self.features = {"image": tf.FixedLenFeature([], tf.string), "label": tf.FixedLenFeature([], tf.float32)}

		tf.reset_default_graph()

	def load_data(self, batch_size=128, root="data", training="training.tfrecord", validation="validation.tfrecord", mean="mean.npy", std="std.npy"):
		""" Defines the tf.data pipeline used for loading the tfrecord files.
			Arguments:
				batch_size: size of batches during training.
				root: path to the tfrecord and mean+std files.
				training: name of the tfrecord for training.
				validation: name of the tfrecord for validation.
				mean: name of the file holding the training set mean.
				std: name of the file holding the training set std.
		"""
		self.batch_size = batch_size

		self.training_files = [os.path.join(root, training)]
		self.validation_files = [os.path.join(root, validation)]

		# Count the number of files in training and validation.
		self.num_training = get_count(self.training_files)
		self.num_validation = get_count(self.validation_files)
		self.num_batch = math.ceil(self.num_training / self.batch_size)

		self.mean = np.load(os.path.join(root, mean))
		self.std = np.load(os.path.join(root, std))

		# Define the tf.data pipeline
		self.files = tf.placeholder(tf.string, shape=[None])
		dataset = tf.data.TFRecordDataset(self.files) # load from tfrecord
		dataset = dataset.shuffle(buffer_size=self.num_training) # shuffle the dataset to ensure batch representation of the comple dataset
		# define function for loading a record from the tfrecord and batch them.
		dataset = dataset.apply(tf.contrib.data.map_and_batch(map_func=self.parse_record, batch_size=self.batch_size, drop_remainder=True))  
		dataset = dataset.prefetch(buffer_size=250)

		self.iterator = dataset.make_initializable_iterator()

	def train(self, epochs=60, lr=0.00025, reg=0.0, continue_training=False):
		""" Train the model.
			Arguments:
				epochs: Number of epochs that the training should last.
				lr: learning rate for training.
				reg: weight of weight decay for training.
				continue_training: boolean whether existing model should be continued.
		"""
		x, y = self.iterator.get_next()
		training = tf.placeholder(tf.bool)
		drop_rate = tf.placeholder(tf.float32)

		# Create the model.
		model = Model(learning_rate=lr, reg=reg, batch_size=self.batch_size, color_space=self.color_space)
		model.build(x=x, y=y, training=training, drop_rate=drop_rate)

		min_val_loss = None
		with tf.Session() as session:
			self.logger.start_training(epochs, self.num_training, self.num_validation, model.learning_rate, model.reg, self.color_space)

			if continue_training:
				model.restore(session, os.path.join(self.model_path, "{0}.ckpt".format(self.model_name)))
			else:
				session.run(tf.global_variables_initializer())
				model.load_mean_and_std(session, self.mean, self.std)
			# Training: 
			for epoch in range(1, epochs+1):
				self.logger.start_epoch(epoch)
				# train the model for one epoch
				train_loss, train_error, train_acc = self.run_epoch(session, self.training_files, model, (model.train, model.metrics), feed_dict={training: True, drop_rate: 0.5})
				# evaluate the model on validation, training:False, drop_rate:0.0
				val_loss, val_error, val_acc = self.run_epoch(session, self.validation_files, model, (model.metrics), feed_dict={training: False, drop_rate: 0.0})
				# If better than previous epochs, save current model
				if min_val_loss is None or val_loss < min_val_loss:
					min_val_loss = val_loss
					model.save(session, os.path.join(self.model_path, "{0}.ckpt".format(self.model_name)))

				self.logger.end_epoch(epoch, train_loss, train_error, train_acc, val_loss, val_error, val_acc)
			# One last evaluation on the best model produced during training (not neccessarily the last)
			model.restore(session, os.path.join(self.model_path, "{0}.ckpt".format(self.model_name)))
			val_loss, val_error, val_acc = self.run_epoch(session, self.validation_files, model, (model.metrics), feed_dict={training: False, drop_rate: 0.0})

			self.logger.end_training(os.path.join(self.model_path, "{0}.ckpt".format(self.model_name)), val_loss, val_error, val_acc)
		self.logger.write_log()

	def run_epoch(self, session, tf_files, model, fetches, feed_dict):
		""" Run over a complete tfrecord file.
			Arguments:
				session: the tf session.
				tf_files: path to the tfrecord files.
				model: model over which to run.
				fetches: tf ops in the model to be calculated.
				feed_dict: input to the model (usually variables for drop rate and training)
			Returns:
				results of mse, mae, accuracy performance metrics for the run.
		"""
		session.run(tf.local_variables_initializer())
		session.run(self.iterator.initializer, feed_dict={self.files: tf_files})
		while True:
			try:
				session.run(fetches, feed_dict=feed_dict)
			except tf.errors.OutOfRangeError:
				break

		loss = model.mean_loss.eval()
		error = model.mean_error.eval()
		acc = model.acc.eval()
		return loss, error, acc


	def parse_record(self, record):
		""" Load a single record from a tfrecord file.
			Arguments:
				record: record to be parsed.
			Returns:
				parsed record.
		"""
		example = tf.parse_single_example(record, self.features)
		image = tf.image.decode_jpeg(example["image"], channels=self.color_space.num_channels)
		# the images are already in 200x66, the tensor size needs just to be defined.
		image = tf.image.resize_images(image, [66, 200])
		# range [0, 1]
		image = image / tf.constant(255.0)
		image = self.color_space.convert(image)
		return (image, [example['label']])

def get_count(tfrecord_files):
	""" Count the number of records in the tfrecord files.
		Arguments:
			tfrecord_files: tfrecord files in which to count.
		Returns:
			Number of records in the files.
	"""
	num = 0
	for file in tfrecord_files:
		for record in tf.python_io.tf_record_iterator(file):
			num += 1
	return num

if __name__ == "__main__":
	if len(sys.argv) > 1:
		root = sys.argv[1]
	else:
		print("Using default root in /data")
		root="data"

	if not os.path.exists("model"):
		os.makedirs("model") 
	trainer = Trainer(model_path="model", model_name="model", color_space=ColorSpace.YUV)
	trainer.load_data(root=root, batch_size=64)
	trainer.train(epochs=60, lr=0.00025, reg=0.001, continue_training=False)
