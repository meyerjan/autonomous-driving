import tensorflow as tf
import numpy as np

from color_space import ColorSpace

class Model:
	""" Defines the adapter PilotNet used in training.
	"""

	def __init__(self, learning_rate=0.00025, reg=0.0001, batch_size=128, color_space=ColorSpace.YUV):
		"""	Initialization.
			Arguments:
				learning_rate: learning rate of training on the model.
				reg: weight of the weight decay.
				batch_size: size of a batch used in training.
				color_space: colorspace of the input images.
		"""
		self.learning_rate = learning_rate
		self.reg = reg
		self.batch_size = batch_size
		self.color_space = color_space

	def build(self, x, y, training, drop_rate):
		""" Builds the model.
			Arguments:
				x: input to the model
				y: label corresponding to the input
				training: variable to define whether the model is being trained or not (for batch-norm).
				drop_rate: drop_rate of dropout layers.
		"""
		# variables for training set mean and std.
		self.mean = tf.get_variable("input_mean", [66, 200, self.color_space.num_channels], dtype=tf.float32, initializer=tf.zeros_initializer, trainable=False)
		self.std = tf.get_variable("input_std", [66, 200, self.color_space.num_channels], dtype=tf.float32, initializer=tf.ones_initializer, trainable=False)

		# normalize the input with mean and std.
		x = tf.divide(tf.subtract(x, self.mean), self.std)

		conv1 = self.conv_layer(inputs=x, filters=24, kernel_size=5, strides=2, training=training, drop_rate=drop_rate)
		conv2 = self.conv_layer(inputs=conv1, filters=36, kernel_size=5, strides=2, training=training, drop_rate=drop_rate)
		conv3 = self.conv_layer(inputs=conv2, filters=48, kernel_size=5, strides=2, training=training, drop_rate=drop_rate)
		conv4 = self.conv_layer(inputs=conv3, filters=64, kernel_size=3, strides=1, training=training, drop_rate=drop_rate)
		conv5 = self.conv_layer(inputs=conv4, filters=64, kernel_size=3, strides=1, training=training, drop_rate=drop_rate)

		flatten = tf.layers.flatten(inputs=conv5)

		# fully-connected layers
		dense1 = self.dense_layer(inputs=flatten, units=100, training=training, drop_rate=drop_rate)
		dense2 = self.dense_layer(inputs=dense1, units=50, training=training, drop_rate=drop_rate)
		dense3 = self.dense_layer(inputs=dense2, units=10, training=training, drop_rate=drop_rate)

		self.y_pred = tf.layers.dense(inputs=dense3, units=1) # network prediction

		# metrics for MAE and accuracy.
		self.mean_error, mean_error_update = tf.metrics.mean_absolute_error(labels=y, predictions=self.y_pred)
		self.acc, acc_update = tf.metrics.mean(tf.to_float(tf.less_equal(tf.abs(tf.subtract(y, self.y_pred)), 0.1)))

		self.loss = tf.losses.mean_squared_error(labels=y, predictions=self.y_pred)
		self.mean_loss, mean_loss_update = tf.metrics.mean(self.loss, name="loss_mean") # metric for loss

		self.metrics = tf.group(mean_error_update, mean_loss_update, acc_update) # update op for metrics

		# weight decay term.
		regularizer = tf.contrib.layers.l2_regularizer(scale=self.reg)
		weights = [w for w in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES) if w.name.endswith('kernel:0')]
		reg_term = tf.contrib.layers.apply_regularization(regularizer, weights)

		global_step = tf.Variable(0, trainable=False)
		optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)

		update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
		with tf.control_dependencies(update_ops):
			# Training step.
			self.train = optimizer.minimize(self.loss + reg_term, global_step=global_step)

		# VisualBackProp:
		deconv5 = self.deconv_layer(input_conv=conv5, input_deconv=None, kernel_size=3, strides=1, output_shape=[3, 20])
		deconv4 = self.deconv_layer(input_conv=conv4, input_deconv=deconv5, kernel_size=3, strides=1, output_shape=[5, 22])
		deconv3 = self.deconv_layer(input_conv=conv3, input_deconv=deconv4, kernel_size=5, strides=2, output_shape=[14, 47])
		deconv2 = self.deconv_layer(input_conv=conv2, input_deconv=deconv3, kernel_size=5, strides=2, output_shape=[31, 98])
		deconv1 = self.deconv_layer(input_conv=conv1, input_deconv=deconv2, kernel_size=5, strides=2, output_shape=[66, 200])

		self.visualization_mask = self.normalize_mask(deconv1) # output of VBProp

		self.saver = tf.train.Saver(max_to_keep=None)

	def load_mean_and_std(self, session, mean, std):
		""" Load the mean and std variables.
			Arguments:
				session: current tf session.
				mean: mean of the training dataset.
				std: std of the training dataset.
		"""

		self.mean.load(mean, session)
		self.std.load(std, session)

	def conv_layer(self, inputs, filters, kernel_size, strides, training, drop_rate):
		""" Define a Convolutional Layer including Batch-Normalization, ReLU and Dropout.
			Arguments:
				inputs: input to the layer.
				filters: number of filters in the layer.
				kernel_size: size of the filter of the layer.
				strides: stride of the layer.
				training: variable whether the layer is being trained.
				dropt_rate: dropout rate of the layer.
			Returns:
				Output of the layer.
		"""
		conv = tf.layers.conv2d(inputs=inputs, filters=filters, kernel_size=kernel_size, strides=strides, activation=None, kernel_initializer=tf.initializers.glorot_normal())
		norm = tf.layers.batch_normalization(inputs=conv, fused=True, training=training)
		relu = tf.nn.relu(norm)
		drop = tf.layers.dropout(inputs=relu, rate=drop_rate)
		return drop

	def dense_layer(self, inputs, units, training, drop_rate):
		""" Define a fully-connected layer including Batch-Normalization, ReLU and Dropout.
			Arguments:
				inputs: input to the layer.
				units: number of neurons in the layer.
				training: variable whether the layer is being trained.
				dropt_rate: dropout rate of the layer.
			Returns:
				Output of the layer.
		"""
		dense = tf.layers.dense(inputs=inputs, units=units, activation=None, kernel_initializer=tf.initializers.glorot_normal())
		norm = tf.layers.batch_normalization(inputs=dense, fused=True, training=training)
		relu = tf.nn.relu(norm)
		drop = tf.layers.dropout(inputs=relu, rate=drop_rate)
		return drop

	def deconv_layer(self, input_conv, input_deconv, kernel_size, strides, output_shape):
		""" Define a deconvolutional layer (VBProp).
			Arguments:
				input_conv: the conv layer that produced the feature cube corresponding to the deconv layer.
				input_deconv: previous deconv layer.
				kernel_size: filter size of the deconv layer (matching the conv).
				strides: stride of the deconv layer (matching the conv).
				output_shape: shape of the output (matching the conv).#
			Returns:
				Output of the VBProp layer.
		"""
		# average of the feature cube produced by the conv layer
		mean_conv = tf.reduce_mean(input_conv, axis=3, keep_dims=True)
		# multiply with previous deconv layer
		mask = tf.multiply(mean_conv, input_deconv) if input_deconv is not None else mean_conv
		filter_shape = [kernel_size, kernel_size, 1, 1]
		# tf.nn is used instead of tf.layers as for the other layers, because tf.layers.conv2d_transpose does not allow the definition of the output-shape, which is ambiguous
		deconv = tf.nn.conv2d_transpose(value=mask, filter=tf.ones(filter_shape), output_shape=tf.stack([self.batch_size, output_shape[0], output_shape[1], 1]), strides=[1, strides, strides, 1], padding='VALID')
		return deconv

	def normalize_mask(self, mask):
		""" Normalize the visualization mask to the range [0, 255] as a single channel color map.
			Arguments:
				mask to be normalized.
			Returns:
				normalized mask.
		"""
		mask_min = tf.reduce_min(mask)
		mask_max = tf.reduce_max(mask)
		mask = tf.divide(tf.subtract(mask, mask_min), tf.subtract(mask_max, mask_min))
		return tf.multiply(mask, tf.constant(255.0)) # * 255.0 to be layerd over rgb image later.

	def save(self, session, model_path):
		""" Save the model parameters and settings.
			Arguments:
				session: the tf.session.
				model_path: path where the model should be stored.
		"""
		self.saver.save(session, model_path)

	def restore(self, session, model_path):
		""" Restores a previously saved model.
			Arguments:
				session: the tf.session.
				model_path: path to the model to be restored.
		"""
		self.saver.restore(session, model_path)
